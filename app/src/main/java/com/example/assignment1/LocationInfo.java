package com.example.assignment1;

/**
 * Helper class to keep track of database entries
 */
public class LocationInfo {
    // Label table name
    public static final String TABLE_NAME = "Locations";

    // Label table attributes
    public static final String KEY_ID = "id";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";

    //keep track of information
    public int location_ID;
    public double latitude;
    public double longitude;

}
