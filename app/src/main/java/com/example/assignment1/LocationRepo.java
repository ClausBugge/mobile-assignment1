package com.example.assignment1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Claus on 19/09/2016.
 */
public class LocationRepo {
    private DBHelper dbHelper;

    public LocationRepo(Context context) {
        dbHelper = new DBHelper(context);
    }

    public int insert(LocationInfo locationInfo) {
        SQLiteDatabase database = dbHelper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(locationInfo.KEY_LATITUDE, locationInfo.latitude);
        values.put(locationInfo.KEY_LONGITUDE, locationInfo.longitude);

        long locationId = database.insert(locationInfo.TABLE_NAME, null, values);

        database.close();
        return (int) locationId;
    }

    public void delete(int locationID)
    {
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        database.delete(LocationInfo.TABLE_NAME, LocationInfo.KEY_ID + "=?", new String[]{String.valueOf(locationID)});
        database.close();
    }

    public void clearDatabase(){
        dbHelper.getWritableDatabase().delete(LocationInfo.TABLE_NAME, null, null);
    }

    public ArrayList<LocationInfo> getLocationList(){
        SQLiteDatabase database = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT " +
                LocationInfo.KEY_ID + ", " +
                LocationInfo.KEY_LATITUDE + ", " +
                LocationInfo.KEY_LONGITUDE +
                " FROM " + LocationInfo.TABLE_NAME;

        ArrayList<LocationInfo> locationList = new ArrayList<LocationInfo>();

        Cursor cursor = database.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                LocationInfo data = new LocationInfo();
                data.location_ID = cursor.getInt(cursor.getColumnIndex(LocationInfo.KEY_ID));
                data.latitude = cursor.getFloat(cursor.getColumnIndex(LocationInfo.KEY_LATITUDE));
                data.longitude = cursor.getFloat(cursor.getColumnIndex(LocationInfo.KEY_LONGITUDE));
                locationList.add(data);
            }while(cursor.moveToNext());
        }
        cursor.close();
        database.close();
        return locationList;
    }
}
