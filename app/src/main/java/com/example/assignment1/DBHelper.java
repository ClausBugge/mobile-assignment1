package com.example.assignment1;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

/**
 * Created by Claus on 19/09/2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 8;

    private static final String DATABASE_NAME = "location.db";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        String CREATE_TABLE_LOCATIONS = "CREATE TABLE " + LocationInfo.TABLE_NAME + "("
                + LocationInfo.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + LocationInfo.KEY_LATITUDE + " FLOAT, "
                + LocationInfo.KEY_LONGITUDE + " FLOAT )";

        database.execSQL(CREATE_TABLE_LOCATIONS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
        database.execSQL("DROP_TABLE_IF_EXISTS " + LocationInfo.TABLE_NAME);

        onCreate(database);
    }
}
