package com.example.assignment1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewDatabase extends AppCompatActivity {

    Button buttonClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_database);

        buttonClose = (Button) findViewById(R.id.buttonClose);

        LocationRepo repo = new LocationRepo(this);

        ArrayList<LocationInfo> locationList = repo.getLocationList();

        if(locationList.size() != 0)
        {

            LinearLayout layout = (LinearLayout) findViewById(R.id.ListLayout);
            layout.removeAllViews();
            for(LocationInfo locationInfo : locationList)
            {
                TextView temp = new TextView(this);
                temp.setText(locationInfo.latitude + ", " + locationInfo.longitude);
                layout.addView(temp);
            }
        }

        //dummy values for testing
        int locationID=2;
        Intent intent = getIntent();
        locationID = intent.getIntExtra("location_Id", 0);

    }

    public void closeActivity(View view){
        finish();
    }
}
