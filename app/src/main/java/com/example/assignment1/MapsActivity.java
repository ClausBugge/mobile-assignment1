package com.example.assignment1;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements
        OnMapReadyCallback,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener {

    final int PERMISSION_FINE_LOCATION = 0;
    final int PERMISSION_COARSE_LOCATION = 0;

    private GoogleMap mMap;

    private Location mLocation;

    private ImageButton mButtonSave;
    private ImageButton mButtonCurrent;
    private ImageButton mButtonView;
    private ImageButton mButtonAddMarkers;
    private ImageButton mButtonTrashDatabase;

    //google api client
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //check if permissions have been allowed
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSION_FINE_LOCATION);

            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION},
                    PERMISSION_COARSE_LOCATION);

            //Toast.makeText(this, "Cant get permissions", Toast.LENGTH_LONG).show();
            //return;
        }


        //Initialize googleApi client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        //initialize location
        /*
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(mLocation != null){
            Toast.makeText(this, mLocation.getLongitude() + ", " + mLocation.getLatitude(), Toast.LENGTH_SHORT).show();

        }else{
            Toast.makeText(this, "Location returned null", Toast.LENGTH_LONG).show();
        }
        */

        //initialize views and adding onclick listeners
        mButtonSave = (ImageButton) findViewById(R.id.buttonSave);
        mButtonCurrent = (ImageButton) findViewById(R.id.buttonCurrent);
        mButtonView = (ImageButton) findViewById(R.id.buttonView);
        mButtonAddMarkers = (ImageButton) findViewById(R.id.buttonAddMarkers);
        mButtonTrashDatabase = (ImageButton) findViewById(R.id.buttonTrashDatabase);

        mButtonSave.setOnClickListener(this);
        mButtonCurrent.setOnClickListener(this);
        mButtonView.setOnClickListener(this);
        mButtonAddMarkers.setOnClickListener(this);
        mButtonTrashDatabase.setOnClickListener(this);
    }

    private void moveMapToCurrentLocation() {
        //update location, permissions granted in the constructor
        //mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null) {
            mMap.clear();
            LatLng latLng = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());

            //add marker
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .draggable(true)
                    .title("Current Location"));

            //move to current location
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f));


        } else {
            Toast.makeText(this, "Location not loaded yet", Toast.LENGTH_LONG).show();
        }
    }

    private void viewLocations() {
        //dummy values for testing
        Intent intent = new Intent(this, ViewDatabase.class);
        intent.putExtra("location_Id", 0);

        startActivity(intent);
    }

    private void saveLocation() {
        if (mLocation != null) {
            LocationRepo repo = new LocationRepo(this);
            LocationInfo loc = new LocationInfo();
            loc.latitude = mLocation.getLatitude();
            loc.longitude = mLocation.getLongitude();
            repo.insert(loc);
            Toast.makeText(this,"Current Location: " +  loc.latitude + ", " + loc.longitude, Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(this, "Location not loaded yet", Toast.LENGTH_LONG).show();
        }
    }

    private void addLocationMarkers() {

        LocationRepo repo = new LocationRepo(this);
        ArrayList<LocationInfo> locationList = repo.getLocationList();

        if(locationList.size() != 0)
        {
            for(LocationInfo locationInfo : locationList)
            {
                LatLng latLng = new LatLng(locationInfo.latitude, locationInfo.longitude);
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .draggable(true)
                        .title("Location Index: " + locationInfo.location_ID));
            }
        }
    }

    private void clearDatabase(){

        final Context context = this;

        new AlertDialog.Builder(this)
                .setTitle("Clear database")
                .setMessage("Are you sure you want to clear the location database?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        mMap.clear();
                        LocationRepo repo = new LocationRepo(context);
                        repo.clearDatabase();
                        Toast.makeText(context, "Database cleared!", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        moveMapToCurrentLocation();


    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "The app does not have correct permissions", Toast.LENGTH_LONG).show();
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        if(mLocation == null)
        {
            mLocation = location;
            moveMapToCurrentLocation();
        }
        mLocation = location;
    }

    @Override
    public void onClick(View v) {
        if (v == mButtonCurrent) {
            moveMapToCurrentLocation();
        }
        if (v == mButtonView) {
            viewLocations();
        }
        if (v == mButtonSave) {
            saveLocation();
        }
        if (v == mButtonAddMarkers){
            addLocationMarkers();
        }
        if (v == mButtonTrashDatabase){
            clearDatabase();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {

    }
}
